package com.mstn.facilidades.server.gcm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

/**
 * Clase para interactuar con GCM
 * @author Jose Suero
 *
 */
public class gcmApi {
	
	/**
	 * Cantidad de dispositivos para enviar multicast
	 */
	private static final int MULTICAST_SIZE = 1000;
	/**
	 * Ejecutor de mensajes 
	 */
	private static final Executor threadPool = Executors.newFixedThreadPool(5);
	/**
	 * Variable del envio
	 */
	private Sender sender;
	/**
	 * Variable con el logger
	 */
	protected final Logger logger = Logger.getLogger(getClass().getName());
	
	/*
	 * MessageTypes
	 * info
	 * newOrders
	 * deleteOrder
	 * 
	 */
	
	
	/**
	 * Constructor 
	 */
	public gcmApi(){
	    //TODO guardar en web.config
		String key = "AIzaSyBaIuzQuKhAIYNqPqe7XXnjBTDdojrWx2Y";
	    sender = new Sender(key);
	}
	
	/**
	 * Funcion para enviar mensaje a disposivos mediante GCM
	 * @param devices Lista de dispositivos
	 * @param messageData Data en JSON del mensaje
	 * @return cantidad de mensajes puesto en cola para enviar
	 */
	public String sendMessage(List<String> devices, JSONObject messageData){
	    String status;
        
	    // send a multicast message using JSON
        // must split in chunks of 1000 devices (GCM limit)
        int total = devices.size();
        List<String> partialDevices = new ArrayList<String>(total);
        int counter = 0;
        int tasks = 0;
        for (String device : devices) {
          counter++;
          partialDevices.add(device);
          int partialSize = partialDevices.size();
          if (partialSize == MULTICAST_SIZE || counter == total) {
            asyncSend(partialDevices,messageData);
            partialDevices.clear();
            tasks++;
          }
        }
        status = "Asynchronously sending " + tasks + " multicast messages to " +
            total + " devices";

	    return status.toString();
	}
	
	/**
	 * Tarea para enviar mensajes  
	 * @param partialDevices Lista dispositivos a enviar
	 * @param messageData data del mensaje
	 */
	private void asyncSend(List<String> partialDevices, JSONObject messageData) {
	    // make a copy
	    final List<String> devices = new ArrayList<String>(partialDevices);
	    final String messData = messageData.toString();
	    threadPool.execute(new Runnable() {

	      public void run() {
	        Message message = new Message.Builder().addData("value", messData).build();
	        MulticastResult multicastResult;
	        try {
	          multicastResult = sender.send(message, devices, 5);
	        } catch (IOException e) {
	          logger.log(Level.SEVERE, "Error posting messages", e);
	          return;
	        }
	        List<Result> results = multicastResult.getResults();
	        // analyze the results
	        for (int i = 0; i < devices.size(); i++) {
	          String regId = devices.get(i);
	          Result result = results.get(i);
	          String messageId = result.getMessageId();
	          if (messageId != null) {
	            logger.fine("Succesfully sent message to device: " + regId +
	                "; messageId = " + messageId);
	            String canonicalRegId = result.getCanonicalRegistrationId();
	            if (canonicalRegId != null) {
	              // same device has more than on registration id: update it
	              logger.info("canonicalRegId " + canonicalRegId);
	              //Datastore.updateRegistration(regId, canonicalRegId);
	            }
	          } else {
	            String error = result.getErrorCodeName();
	            if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
	              // application has been removed from device - unregister it
	              logger.info("Unregistered device: " + regId);
	              //Datastore.unregister(regId);
	            } else {
	              logger.severe("Error sending message to " + regId + ": " + error);
	            }
	          }
	        }
	      }});
	  }
}
