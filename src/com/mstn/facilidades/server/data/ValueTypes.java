package com.mstn.facilidades.server.data;

/**
 * Enumeracion con lista de tipo de valores 
 * @author Jose Suero
 *
 */
public enum ValueTypes {
	String,number,date
}
