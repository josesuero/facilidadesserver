package com.mstn.facilidades.server.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.*;
import javax.sql.DataSource;

import com.sun.rowset.CachedRowSetImpl;

/**
 * Clase de interaccion con base de datos 
 * @author Jose Suero
 *
 */
public class WebData {

	/**
	 * Obtener coneccion de la base de datos
	 * @return conneccion de base de datos
	 * @throws NamingException
	 * @throws SQLException
	 */
	public Connection getConnection() throws NamingException, SQLException{
		
		   Context ctx = null;

		    DataSource ds = null;
		         
		    Hashtable<String,String> ht = new Hashtable<String,String>( );

		    ht.put(Context.INITIAL_CONTEXT_FACTORY,
		      "weblogic.jndi.WLInitialContextFactory");

		    ht.put(Context.PROVIDER_URL,"t3://localhost:7001");
		    
		    ctx = new InitialContext(ht);
		    ds = (DataSource) ctx.lookup ("APPCELTEC");
		    Connection conn = ds.getConnection();
		    return conn;
	}

	/**
	 * Ejecuta un query sin parametros
	 * @param sql String con el query a ejecutar
	 * @return CachedRowset con los datos
	 * @throws SQLException
	 * @throws NamingException
	 */
	public CachedRowSetImpl query(String sql) throws SQLException, NamingException{
		return query(sql,new ArrayList<DataParam>());
	}
	
	/**
	 * Ejecuta un query con parametros 
	 * @param sql Query string a ejecutar
	 * @param Valores a reemplazar en el query
	 * @return CachedRowset con los datos
	 * @throws SQLException
	 * @throws NamingException
	 */
	public CachedRowSetImpl query(String sql,List<DataParam> values) throws SQLException, NamingException{
		Connection conn = getConnection();
		try{
			PreparedStatement stmt = conn.prepareStatement(sql);
			for(int i = 0; i < values.size();i++){
				DataParam item = values.get(i);
				switch(item.getType()){
				case String:
					stmt.setString(i+1, item.getValue());
					break;
				case date:
					stmt.setDate(i+1, Date.valueOf(item.getValue()));
					break;
				case number:
					stmt.setInt(i+1, Integer.valueOf(item.getValue()));
					break;
				default:
					break;
				}
			}
			stmt.executeQuery();
			ResultSet rs = stmt.getResultSet(); 
		    CachedRowSetImpl crs = new CachedRowSetImpl();
	        crs.populate(rs);
	        rs.close();
			return crs;
		} finally {
			
			conn.close();
		}
	}

	/**
	 * Ejecuta una actualizacion (update/delete) a una tabla
	 * @param sql String del query
	 * @param values Valores a reemplazar
	 * @throws NamingException
	 * @throws SQLException
	 */
	public void noquery(String sql,List<DataParam> values) throws NamingException, SQLException{
		Connection conn = getConnection();
		try{
		PreparedStatement stmt = conn.prepareStatement(sql);
		for(int i = 0; i < values.size();i++){
			DataParam item = values.get(i);
			switch(item.getType()){
			case String:
				stmt.setString(i+1, item.getValue());
				break;
			case date:
				stmt.setDate(i+1, Date.valueOf(item.getValue()));
				break;
			case number:
				stmt.setInt(i+1, Integer.valueOf(item.getValue()));
				break;
			default:
				break;
			}
		}
		stmt.executeUpdate();
		} finally {
			conn.close();
		}
	}
}
