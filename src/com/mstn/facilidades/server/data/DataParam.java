package com.mstn.facilidades.server.data;

/**
 * Clase para parametros en query /updates de datos en tabla
 * @author Jose Suero
 *
 */
public class DataParam {
	/**
	 * Valor del campo
	 */
	private String value;
	/**
	 * Tipo del campo
	 */
	private ValueTypes type;
	/**
	 * Getter de valor
	 * @return
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Constructor
	 */
	public DataParam(){
		
	}
	/**
	 * Constructor
	 * @param type tipo de campo
	 * @param value valor del campo
	 */
	public DataParam(ValueTypes type, String value){
		this.type = type;
		this.value = value;
	}
	/**
	 * Setter de valor
	 * @param value valor
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * Getter de tipo
	 * @return
	 */
	public ValueTypes getType() {
		return type;
	}
	/**
	 * Setter de tipo
	 * @param type
	 */
	public void setType(ValueTypes type) {
		this.type = type;
	}
}
