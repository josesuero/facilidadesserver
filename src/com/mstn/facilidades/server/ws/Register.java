package com.mstn.facilidades.server.ws;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;
import com.sun.rowset.CachedRowSetImpl;

/**
 * Servicio para registrar un dispositivo en el servidor
 * @author Jose Suero
 *
 */
@Path("/register")
public class Register {

	// This method is called if TEXT_PLAIN is request

	/**
	 * responde al metodo GET
	 * @param tarjeta tarjeta del usuario
	 * @param regId registro del dispositivo en GCM
	 * @return resultado
	 */
	@GET
	public String GetRegisterDevice(@QueryParam("tarjeta") String tarjeta,
			@QueryParam("regId") String regId) {
		return RegisterDevice(tarjeta, regId);
	}
	/**
	 * responde al metodo POST
	 * @param tarjeta tarjeta del usuario
	 * @param regId registro del dispositivo en GCM
	 * @return resultado
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String RegisterDevice(@FormParam("tarjeta") String tarjeta,
			@FormParam("regId") String regId) {
		
		//Estados:
		//1 - Exitoso
		//2 - Repetido
		//3 - Invalido
		
		WebData data = new WebData();
		
		JSONObject rtn = new JSONObject();

		ArrayList<DataParam> values = new ArrayList<DataParam>();

		// id_telefono
		values.add(new DataParam(ValueTypes.String, regId));
		// tarjeta
		values.add(new DataParam(ValueTypes.String, tarjeta));
		// fecha_ini
		values.add(new DataParam(ValueTypes.date, "2013-01-01"));
		// fecha_fin
		values.add(new DataParam(ValueTypes.date, "2023-12-31"));
		// fecha_actual - sysdate
		// actualizado
		// activo - 0

		try {
			data.noquery(
					"insert into clientes(id_telefono,tarjeta,fecha_ini,fecha_fin,actualizado,activo) values(?,?,?,?,'Server',0)",
					values);
			rtn.put("status", 1);
			rtn.put("message", "Registro Exitoso");
		} catch (SQLException e) {
			if (e.getErrorCode() == 1 ||e.getErrorCode() == 1062){
				try {
					values = new ArrayList<DataParam>();
					values.add(new DataParam(ValueTypes.String, regId));
					values.add(new DataParam(ValueTypes.String, tarjeta));

					CachedRowSetImpl rs = data.query("select tarjeta from clientes where id_telefono = ? and tarjeta = ?",values);
					if (rs.next()){
						rtn.put("status", 2);
						rtn.put("message", "Registro Duplicado");
					} else {
						rtn.put("status", 3);
						rtn.put("message", "Registro Invalido");
					}
				} catch (JSONException e1) {
					return e1.toString();
				} catch (Exception e1){
					return e1.toString();
				}
			}
		} catch (Exception e){
			return e.toString();
		}

		return rtn.toString();
	}
}