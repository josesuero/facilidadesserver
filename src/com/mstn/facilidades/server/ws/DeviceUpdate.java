package com.mstn.facilidades.server.ws;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;

/**
 * Servicio para actualizar data del dispositivo en la table
 * @author Jose Suero
 *
 */
@Path("/DeviceUpdate")
public class DeviceUpdate {

	/**
	 * Informacion del Request
	 */
	@Context HttpServletRequest req;
	
	/**
	 * Responde al evento POST
	 * @param regId Registro del dispositivo
	 * @param tarjeta Tarjeta del usuario
	 * @param fecha_ini Fecha Inicial
	 * @param fecha_fin Fecha Final
	 * @param activo Activo
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String update(@FormParam("id_telefono") String regId, @FormParam("tarjeta") String tarjeta, @FormParam("fecha_ini") String fecha_ini, @FormParam("fecha_fin") String fecha_fin, @FormParam("activo") String activo){
		
		WebData data = new WebData();
		
		JSONObject rtn = new JSONObject();
		
		ArrayList<DataParam> values = new ArrayList<DataParam>();

		HttpSession session= req.getSession(true);
		
		//String actualizado = session.getAttribute("user").toString();
		String actualizado = session.getId(); 
		
		values.add(new DataParam(ValueTypes.date, fecha_ini));
		if (fecha_fin.equals("")){
			values.add(new DataParam(ValueTypes.date, "2999-12-31"));
		} else {
			values.add(new DataParam(ValueTypes.date, fecha_fin));	
		}
		
		values.add(new DataParam(ValueTypes.String, actualizado));
		if (activo.equals("false"))
			values.add(new DataParam(ValueTypes.number, "0"));
		else if (activo.equals("true"))
			values.add(new DataParam(ValueTypes.number, "1"));

		values.add(new DataParam(ValueTypes.String, regId));
		values.add(new DataParam(ValueTypes.String, tarjeta));

		try {
			data.noquery(
					"update clientes set fecha_ini = ?,fecha_fin = ?,actualizado = ?,activo = ? where id_telefono = ? and tarjeta = ?",
					values);
			rtn.put("status", 1);
			rtn.put("message", "Registro Exitoso");
		} catch (Exception e){
			return e.toString();
		}

		return rtn.toString();
	}
}
