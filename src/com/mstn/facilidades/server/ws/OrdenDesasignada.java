package com.mstn.facilidades.server.ws;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Modelo de xml para ordenes deasignadas
 * @author Jose Suero
 *
 */
@XmlRootElement
public class OrdenDesasignada {
	/**
	 * Tarjeta del tecnico
	 */
	private String TarjetaTecnico;
	/**
	 * Numero de orden
	 */
	private String NoOrden;
	
	/**
	 * Constructor
	 */
	public OrdenDesasignada(){
		
	}
	/**
	 * Constructor
	 * @param TarjetaTecnico tarjeta del tecnico
	 * @param NoOrden numero de orden
	 */
	public OrdenDesasignada(String TarjetaTecnico, String NoOrden){
		this.TarjetaTecnico = TarjetaTecnico;
		this.NoOrden = NoOrden;
	}

	/**
	 * Getter tarjeta tecnico
	 * @return
	 */
	public String getTarjetaTecnico() {
		return TarjetaTecnico;
	}

	/**
	 * Setter tarjeta tecnico
	 * @param tarjetaTecnico
	 */
	public void setTarjetaTecnico(String tarjetaTecnico) {
		TarjetaTecnico = tarjetaTecnico;
	}

	/**
	 * Getter numero Orden
	 * @return
	 */
	public String getNoOrden() {
		return NoOrden;
	}
	
	/**
	 * Setter Numero de orden
	 * @param noOrden
	 */
	public void setNoOrden(String noOrden) {
		NoOrden = noOrden;
	}
}
