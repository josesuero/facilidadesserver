package com.mstn.facilidades.server.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/checkVersion")
public class checkVersion {

	@GET
	public String get(){
		return "1.0";
	}
	
	@POST
	public String post(){
		return get();
	}
}
