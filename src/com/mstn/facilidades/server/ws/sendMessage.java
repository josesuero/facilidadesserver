package com.mstn.facilidades.server.ws;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mstn.facilidades.server.gcm.gcmApi;

/**
 * Clase para enviar mensaje al dispositivo
 * @author Jose Suero
 *
 */
@Path("/sendMessage")
public class sendMessage {
	
	/**
	 * Funcion responde al metodo post
	 * @param device registro del dispositivo en GCM
	 * @param message Mensaje a enviar
	 * @param type Tipo de mensaje
	 * @param data Data del mensaje
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String send(@FormParam("device") String device, @FormParam("message") String message, @FormParam("type") String type, @FormParam("data") String data){
		gcmApi gcm = new gcmApi();
		ArrayList<String> devices = new ArrayList<String>();
		devices.add(device);
		JSONObject mess = new  JSONObject();
		
		JSONObject res = new JSONObject();
		try {
			mess.put("message", message);
			mess.put("type", type);
			mess.put("data", new JSONArray(data));
			res.put("status", gcm.sendMessage(devices, mess));
		} catch (JSONException e) {
			return e.toString();
		}
		return res.toString();
	}
}
