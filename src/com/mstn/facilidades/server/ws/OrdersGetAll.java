package com.mstn.facilidades.server.ws;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.mstn.facilidades.server.Utils;

/**
 * Servicio para solicitar las ordenes de un tecnico, estas son solicitadas al servicio de saydot de ordenes
 * @author Jose Suero
 *
 */
@Path("/OrdersGetAll")
public class OrdersGetAll {
	
	@Context
	ServletContext context;

	/**
	 * Responde al evento post
	 * @param tarjeta tarjeta del usuario
	 * @param regId registro del dispositivo
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String ordersPost(@FormParam("tarjeta") String tarjeta, @FormParam("regId") String regId){

		String result = null;
		try
		{
			/*
			if (!Utils.checkDevice(tarjeta, regId)){
				throw new Exception("Dispositivo no autorizado");
			}
			
			//TODO: Llamar webservice getall

			StringBuilder uri = new StringBuilder("http://b0wszk1/OrdenesAsignadas/api/OrdenesAsignadas/ObtenerOrdenesTecnico?tarjeta=");
			uri.append(tarjeta);
	
			
			// Send a GET request to the servlet
			URL url = new URL(uri.toString());
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("GET");
			InputStream ins = conn.getInputStream();
			
			*/	
			InputStream in = context.getResourceAsStream("OrdenesGet.html");
			
			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(in));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null)
			{
			sb.append(line);
			}
			rd.close();
			result = sb.toString();

		} catch (Exception e){
			result = e.getMessage();
		}
		return result;
	}
	
	/**
	 * Responde al evento GET
	 * @param tarjeta tarjeta del usuario
	 * @param regId registro del dispositivo
	 * @return
	 */
	@GET
	public String ordersGet(@QueryParam("tarjeta") String tarjeta, @QueryParam("regId") String regId){
		return ordersPost(tarjeta,regId);
	}
}
