package com.mstn.facilidades.server.ws;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.mstn.facilidades.server.Utils;

@Path("/OrdersUpdate")
public class OrdersUpdate {
	
	@Context
	ServletContext context;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String update(@FormParam("tarjeta") String tarjeta, @FormParam("regId") String regId, @FormParam("data") String data){
		String result = null;
		try{
			if (!Utils.checkDevice(tarjeta, regId)){
				throw new Exception("Dispositivo no autorizado");
			}
			
			//JSONArray datajs = new JSONArray(data);
			//TODO averiguar Parameter name EnviarOrdenesRecibidas
			
			StringBuilder uri = new StringBuilder(context.getInitParameter("OrdersUpdate"));
			//uri.append(data);
			
	        URL url = null;
            url = new URL(uri.toString());

	        HttpURLConnection urlConn = null;
            urlConn = (HttpURLConnection) url.openConnection();

	        urlConn.setDoInput (true);
	        urlConn.setDoOutput (true);
	        urlConn.setUseCaches (false);
            urlConn.setRequestMethod("POST");
            urlConn.connect();

	        DataOutputStream output = null;

            output = new DataOutputStream(urlConn.getOutputStream());

	        // Specify the content type if needed.
	        //urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

	        // Construct the POST data.
	        String content =
	          "data=" + URLEncoder.encode(data.toString(),"UTF-8");


	        // Send the request data.
            output.writeBytes(content);
            output.flush();
            output.close();

			// Get the response

            BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null)
			{
			sb.append(line);
			}
			rd.close();
			result = sb.toString();

			return result;
		} catch(Exception e){
			return e.toString();
		}
	}
	
	@GET
	public String updateGet(@QueryParam("tarjeta") String tarjeta, @QueryParam("regId") String regId, @QueryParam("data") String data){
		return update(tarjeta,regId,data);
	}
}