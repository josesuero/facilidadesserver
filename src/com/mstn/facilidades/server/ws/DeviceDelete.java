package com.mstn.facilidades.server.ws;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;

/**
 * Servicio para eliminar un dispositivo de la tabla
 * @author Jose Suero
 *
 */
@Path("DeviceDelete")
public class DeviceDelete {

	/**
	 * Responde al evento POST
	 * @param regId Registro del dispositivo en GCM
	 * @param tarjeta Tarjeta del usuario
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String delete(@FormParam("id_telefono") String regId, @FormParam("tarjeta") String tarjeta){
		
		WebData data = new WebData();
		
		JSONObject rtn = new JSONObject();
		
		ArrayList<DataParam> values = new ArrayList<DataParam>();
		
		values.add(new DataParam(ValueTypes.String, regId));
		values.add(new DataParam(ValueTypes.String, tarjeta));

		try {
			data.noquery(
					"delete from clientes where id_telefono = ? and tarjeta = ?",
					values);
			rtn.put("status", 1);
			rtn.put("message", "Registro Exitoso");
		} catch (Exception e){
			return e.toString();
		}

		return rtn.toString();
	}
}
