package com.mstn.facilidades.server.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.rowset.CachedRowSet;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;
import com.mstn.facilidades.server.gcm.gcmApi;

/**
 * Servicio para que saydot notifique los tecnicos con ordenes nuevas
 * @author Jose Suero
 *
 */
@Path("/NotificarAsignacionesNuevas")
public class NotificarAsignacionesNuevas {

	/**
	 * Responde al evento Post
	 * @param listaTarjetas JSONArray con tarjetas de tecnicos
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String notificar(@FormParam("tarjetas") String listaTarjetas) {
		ArrayList<String> devices = new ArrayList<String>();
		try {
			JSONArray tarjetas = new JSONArray(listaTarjetas);
			WebData data = new WebData();
			int n = tarjetas.length() - 1;
			String s = ",?";

			StringBuilder lista = new StringBuilder(
					"select id_telefono from clientes where activo = 1 and fecha_ini <= ? and fecha_fin >= ? and tarjeta in (?");
			if (tarjetas.length() > 1) {
				lista.append(String.format(String.format("%%0%dd", n), 0)
						.replace("0", s));
			}

			lista.append(")");

			ArrayList<DataParam> values = new ArrayList<DataParam>();

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();

			values.add(new DataParam(ValueTypes.date, dateFormat.format(date)));
			values.add(new DataParam(ValueTypes.date, dateFormat.format(date)));

			for (int i = 0; i < tarjetas.length(); i++) {
				values.add(new DataParam(ValueTypes.String, (String) tarjetas
						.get(i)));
			}

			CachedRowSet deviceData = data.query(lista.toString(), values);

			while (deviceData.next()) {
				devices.add(deviceData.getString(1));
			}
		} catch (Exception e1) {
			return e1.toString();
		}

		gcmApi gcm = new gcmApi();
		JSONObject mess = new JSONObject();

		JSONObject res = new JSONObject();
		try {
			mess.put("message", "Sus ordenes fueron actualizadas");
			mess.put("type", "newOrders");
			mess.put("data", new JSONArray("[]"));
			res.put("status", gcm.sendMessage(devices, mess));
		} catch (JSONException e) {
			return e.toString();
		}
		return res.toString();
	}

	/**
	 * Responde al evento GET
	 * @param tarjetas
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String notificationGet(@QueryParam("tarjetas") String tarjetas) {
		return notificar(tarjetas);
	}
}
