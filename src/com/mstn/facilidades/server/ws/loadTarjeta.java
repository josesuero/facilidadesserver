package com.mstn.facilidades.server.ws;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;
import com.sun.rowset.CachedRowSetImpl;

/**
 * Servicio para obtener datos de una registro de usuario
 * @author Jose Suero
 *
 */
@Path("/loadTarjeta")
public class loadTarjeta {
	/**
	 * Responde al evento post
	 * @param tarjeta tarjeta del usuario
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public String notificar(@FormParam("tarjeta") String tarjeta){
		JSONObject dataTarjeta = new JSONObject();
		WebData data = new WebData();
		ArrayList<DataParam> params = new ArrayList<DataParam>();
		params.add(new DataParam(ValueTypes.String,tarjeta));
		try {
			
			CachedRowSetImpl rs = data.query("select * from clientes where tarjeta = ?",params);
			if (rs.next()){
				dataTarjeta.put("id_telefono", rs.getString("id_telefono"));
				dataTarjeta.put("tarjeta", rs.getString("tarjeta"));
				dataTarjeta.put("fecha_ini", rs.getString("fecha_ini"));
				dataTarjeta.put("fecha_fin", rs.getString("fecha_fin"));
				dataTarjeta.put("fecha_actual", rs.getString("fecha_actual"));
				dataTarjeta.put("actualizado", rs.getString("actualizado"));
				dataTarjeta.put("activo", rs.getString("activo"));
				dataTarjeta.put("status", 200);
			} else {
				dataTarjeta.put("status", 404);
			}
		} catch(Exception e){
			try {
				dataTarjeta.put("status", 500);
				dataTarjeta.put("message", e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return dataTarjeta.toString(); 
	}

}
