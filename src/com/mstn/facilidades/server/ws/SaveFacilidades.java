package com.mstn.facilidades.server.ws;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;

/**
 * Servicio para guardar las facilidades de una orden
 * @author josesuero
 *
 */
@Path("/SaveFacilidades")
public class SaveFacilidades {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String save(@FormParam("tarjeta") String tarjeta, @FormParam("regId") String regId,@FormParam("data") String dataparam){
		try{
			WebData database = new WebData();
			JSONObject data = new JSONObject(dataparam);
			String currdate = new SimpleDateFormat("yyyy-MM-dd",Locale.US).format(new Date());
			data.put("STATUS","1");
			data.put("FECHASTATUS",currdate);
			data.put("CONSUMIDO", "0");
			data.put("FECHACONSUMIDO","");
			
			StringBuilder sqlstr = new StringBuilder("INSERT INTO facilidades.TRABAJOS(NUMEROTRABAJO,TIPOTRABAJO,TIPOFACILIDADES,FECHA,TELEFONO,CABINA,PARFEEDER,TERMINAL,PARLOCAL,TIPOPUERTO,LOCALIDAD,DSLAM,PUERTO,TERMINALFO,DIRECCION,DETALLETRABAJO,STATUS,FECHASTATUS,CONSUMIDO)");
			sqlstr.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			ArrayList<DataParam> values = new ArrayList<DataParam>();
			values.add(new DataParam(ValueTypes.String,data.getString("NUMEROTRABAJO")));
			values.add(new DataParam(ValueTypes.number,data.getString("TIPOTRABAJO")));
			values.add(new DataParam(ValueTypes.number,data.getString("TIPOFACILIDADES")));
			values.add(new DataParam(ValueTypes.date,data.getString("FECHA")));
			values.add(new DataParam(ValueTypes.String,data.getString("TELEFONO")));
			values.add(new DataParam(ValueTypes.String,data.getString("CABINA")));
			values.add(new DataParam(ValueTypes.String,data.getString("PARFEEDER")));
			values.add(new DataParam(ValueTypes.String,data.getString("TERMINAL")));
			values.add(new DataParam(ValueTypes.String,data.getString("PARLOCAL")));
			values.add(new DataParam(ValueTypes.String,data.getString("TIPOPUERTO")));
			values.add(new DataParam(ValueTypes.String,data.getString("LOCALIDAD")));
			values.add(new DataParam(ValueTypes.String,data.getString("DSLAM")));
			values.add(new DataParam(ValueTypes.String,data.getString("PUERTO")));
			values.add(new DataParam(ValueTypes.String,data.getString("TERMINALFO")));
			
			if (data.getBoolean("DIRECCION")){
				values.add(new DataParam(ValueTypes.String,"1"));
			} else {
				values.add(new DataParam(ValueTypes.String,"0"));
			}
			
			values.add(new DataParam(ValueTypes.String,data.getString("DETALLETRABAJO")));
			values.add(new DataParam(ValueTypes.number,data.getString("STATUS")));
			values.add(new DataParam(ValueTypes.date,data.getString("FECHASTATUS")));
			values.add(new DataParam(ValueTypes.number,data.getString("CONSUMIDO")));
			
						

			//values.add(new DataParam(ValueTypes.date,data.getString("FECHACONSUMIDO")));
		
			database.noquery(sqlstr.toString(), values);
		} catch(Exception e){
			return e.toString();	
		}
		return "true";
		
	}
}
