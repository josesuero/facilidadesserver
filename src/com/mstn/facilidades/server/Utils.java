package com.mstn.facilidades.server;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.NamingException;
import javax.sql.rowset.CachedRowSet;

import com.mstn.facilidades.server.data.DataParam;
import com.mstn.facilidades.server.data.ValueTypes;
import com.mstn.facilidades.server.data.WebData;

/**
 * Clase de variable y funciones comunes a otras clases
 * @author Jose Suero
 *
 */
public class Utils {

	/**
	 * Verificar si el dispositivo esta registrado en la base de datos
	 * @param tarjeta Tarjeta del usuario
	 * @param regId Registro del celular
	 * @return Devuelve true si esta registrado false si no es valido
	 * @throws SQLException
	 * @throws NamingException
	 */
	public static boolean checkDevice(String tarjeta, String regId) throws SQLException, NamingException{
		WebData data = new WebData();
		StringBuilder lista = new StringBuilder("select tarjeta from clientes where activo = 1 and fecha_ini <= ? and fecha_fin >= ? and tarjeta = ? and id_telefono = ?");
		ArrayList<DataParam> values = new ArrayList<DataParam>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();

		values.add(new DataParam(ValueTypes.date, dateFormat.format(date)));
		values.add(new DataParam(ValueTypes.date, dateFormat.format(date)));
		values.add(new DataParam(ValueTypes.String, tarjeta));
		values.add(new DataParam(ValueTypes.String, regId));

		CachedRowSet deviceData = data.query(lista.toString(), values);

		if (deviceData.size() == 0){
			return false;
		}
		return true;
	}
}
