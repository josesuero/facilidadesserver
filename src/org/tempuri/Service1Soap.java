/**
 * Service1Soap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface Service1Soap extends java.rmi.Remote {
    public boolean sendMail(java.lang.String desde, java.lang.String hasta, java.lang.String asunto, java.lang.String mensaje) throws java.rmi.RemoteException;
    public boolean DBConnect() throws java.rmi.RemoteException;
    public boolean checkUser(java.lang.String usr, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean checkPassword(java.lang.String user, java.lang.String pass) throws java.rmi.RemoteException;
    public boolean siTienePassword(java.lang.String user) throws java.rmi.RemoteException;
    public java.lang.String fndPswd(java.lang.String user) throws java.rmi.RemoteException;
    public java.lang.String[] getParamSecurity() throws java.rmi.RemoteException;
    public boolean eliminaLogs(java.lang.String v_fec1, java.lang.String v_fec2, java.lang.String v_app, java.lang.String v_usu) throws java.rmi.RemoteException;
    public boolean cambiaPass(java.lang.String v_usu, java.lang.String pass) throws java.rmi.RemoteException;
    public java.lang.String[] getUserInf(java.lang.String v_usu) throws java.rmi.RemoteException;
    public boolean grabaHistPsw(java.lang.String v_usu, java.lang.String v_pass) throws java.rmi.RemoteException;
    public boolean delPrimerPass(int v_usu) throws java.rmi.RemoteException;
    public boolean usoPassword(java.lang.String v_usu, java.lang.String v_pass) throws java.rmi.RemoteException;
    public java.lang.String[] buscaNoticias() throws java.rmi.RemoteException;
    public boolean insertaLogUsu(java.lang.String v_usu, java.lang.String v_msg, java.lang.String v_app, java.lang.String v_IP) throws java.rmi.RemoteException;
    public boolean autentica(java.lang.String v_Usr, java.lang.String v_App, java.lang.String v_pass) throws java.rmi.RemoteException;
    public java.lang.String updateUser(java.lang.String v_usu, java.lang.String v_nombre, java.lang.String v_tar, java.lang.String v_tipo, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean deleteUser(int v_usu, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public java.lang.String strConexion() throws java.rmi.RemoteException;
    public boolean insertaAccesoApps(int v_usu, java.lang.String v_apps, java.lang.String v_rol, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean eliminaAccesoApps(int v_usu, java.lang.String v_apps, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean updateApps(java.lang.String v_app, java.lang.String v_nombre, java.lang.String v_link, java.lang.String v_image, java.lang.String v_desc, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean eliminaApps(java.lang.String v_apps, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean updateNoticia(java.lang.String v_sec, java.lang.String v_ini, java.lang.String v_fin, java.lang.String v_not, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean deleteNoticia(int v_sec, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean updateMenuApps(java.lang.String v_id, java.lang.String v_nom, java.lang.String v_desc, java.lang.String v_tipo, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean deleteMenuApps(java.lang.String v_idApp, java.lang.String v_nom, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean updateRolApps(java.lang.String v_App, java.lang.String v_rol, java.lang.String v_nom, java.lang.String v_st, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean deleteRolApps(java.lang.String v_idApp, java.lang.String v_rol, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public boolean accesoOpcion(java.lang.String v_app, int v_rol, java.lang.String v_opc) throws java.rmi.RemoteException;
    public java.lang.String[] buscaAccesos(java.lang.String v_App, int v_Rol) throws java.rmi.RemoteException;
    public java.lang.String[] buscaAccesosDesc(java.lang.String v_App, int v_Rol) throws java.rmi.RemoteException;
    public int buscaRol(java.lang.String v_App, java.lang.String v_Usu) throws java.rmi.RemoteException;
    public boolean insertaAcceso(java.lang.String v_App, int v_rol, java.lang.String[] v_opc) throws java.rmi.RemoteException;
    public boolean updateParamSec(int v_exp, int v_ses, int v_sus, int v_int, int v_avi, int v_car1, int v_car2, int v_log, java.lang.String v_usu, java.lang.String v_ip) throws java.rmi.RemoteException;
    public java.lang.String[] getUsersApps(java.lang.String v_app) throws java.rmi.RemoteException;
    public boolean updateCuenta(java.lang.String v_usu, java.lang.String v_celular, java.lang.String v_email, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException;
    public java.lang.String[] buscaAccesosDesarrollador(java.lang.String v_App) throws java.rmi.RemoteException;
    public boolean insertaDesarrollador(java.lang.String v_App, java.lang.String[] v_ingenieros, int cant) throws java.rmi.RemoteException;
    public boolean autenticaToken(java.lang.String v_usr, java.lang.String v_app, long v_token) throws java.rmi.RemoteException;
    public java.lang.String URLRetorno(java.lang.String v_usr) throws java.rmi.RemoteException;
}
