package org.tempuri;

public class Service1SoapProxy implements org.tempuri.Service1Soap {
  private String _endpoint = null;
  private org.tempuri.Service1Soap service1Soap = null;
  
  public Service1SoapProxy() {
    _initService1SoapProxy();
  }
  
  public Service1SoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initService1SoapProxy();
  }
  
  private void _initService1SoapProxy() {
    try {
      service1Soap = (new org.tempuri.Service1Locator()).getService1Soap();
      if (service1Soap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)service1Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)service1Soap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (service1Soap != null)
      ((javax.xml.rpc.Stub)service1Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.Service1Soap getService1Soap() {
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap;
  }
  
  public boolean sendMail(java.lang.String desde, java.lang.String hasta, java.lang.String asunto, java.lang.String mensaje) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.sendMail(desde, hasta, asunto, mensaje);
  }
  
  public boolean DBConnect() throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.DBConnect();
  }
  
  public boolean checkUser(java.lang.String usr, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.checkUser(usr, v_usuario, v_ip);
  }
  
  public boolean checkPassword(java.lang.String user, java.lang.String pass) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.checkPassword(user, pass);
  }
  
  public boolean siTienePassword(java.lang.String user) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.siTienePassword(user);
  }
  
  public java.lang.String fndPswd(java.lang.String user) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.fndPswd(user);
  }
  
  public java.lang.String[] getParamSecurity() throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getParamSecurity();
  }
  
  public boolean eliminaLogs(java.lang.String v_fec1, java.lang.String v_fec2, java.lang.String v_app, java.lang.String v_usu) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.eliminaLogs(v_fec1, v_fec2, v_app, v_usu);
  }
  
  public boolean cambiaPass(java.lang.String v_usu, java.lang.String pass) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.cambiaPass(v_usu, pass);
  }
  
  public java.lang.String[] getUserInf(java.lang.String v_usu) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getUserInf(v_usu);
  }
  
  public boolean grabaHistPsw(java.lang.String v_usu, java.lang.String v_pass) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.grabaHistPsw(v_usu, v_pass);
  }
  
  public boolean delPrimerPass(int v_usu) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.delPrimerPass(v_usu);
  }
  
  public boolean usoPassword(java.lang.String v_usu, java.lang.String v_pass) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.usoPassword(v_usu, v_pass);
  }
  
  public java.lang.String[] buscaNoticias() throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.buscaNoticias();
  }
  
  public boolean insertaLogUsu(java.lang.String v_usu, java.lang.String v_msg, java.lang.String v_app, java.lang.String v_IP) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.insertaLogUsu(v_usu, v_msg, v_app, v_IP);
  }
  
  public boolean autentica(java.lang.String v_Usr, java.lang.String v_App, java.lang.String v_pass) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.autentica(v_Usr, v_App, v_pass);
  }
  
  public java.lang.String updateUser(java.lang.String v_usu, java.lang.String v_nombre, java.lang.String v_tar, java.lang.String v_tipo, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateUser(v_usu, v_nombre, v_tar, v_tipo, v_usuario, v_ip);
  }
  
  public boolean deleteUser(int v_usu, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.deleteUser(v_usu, v_usuario, v_ip);
  }
  
  public java.lang.String strConexion() throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.strConexion();
  }
  
  public boolean insertaAccesoApps(int v_usu, java.lang.String v_apps, java.lang.String v_rol, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.insertaAccesoApps(v_usu, v_apps, v_rol, v_usuario, v_ip);
  }
  
  public boolean eliminaAccesoApps(int v_usu, java.lang.String v_apps, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.eliminaAccesoApps(v_usu, v_apps, v_usuario, v_ip);
  }
  
  public boolean updateApps(java.lang.String v_app, java.lang.String v_nombre, java.lang.String v_link, java.lang.String v_image, java.lang.String v_desc, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateApps(v_app, v_nombre, v_link, v_image, v_desc, v_usuario, v_ip);
  }
  
  public boolean eliminaApps(java.lang.String v_apps, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.eliminaApps(v_apps, v_usuario, v_ip);
  }
  
  public boolean updateNoticia(java.lang.String v_sec, java.lang.String v_ini, java.lang.String v_fin, java.lang.String v_not, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateNoticia(v_sec, v_ini, v_fin, v_not, v_usuario, v_ip);
  }
  
  public boolean deleteNoticia(int v_sec, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.deleteNoticia(v_sec, v_usuario, v_ip);
  }
  
  public boolean updateMenuApps(java.lang.String v_id, java.lang.String v_nom, java.lang.String v_desc, java.lang.String v_tipo, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateMenuApps(v_id, v_nom, v_desc, v_tipo, v_usuario, v_ip);
  }
  
  public boolean deleteMenuApps(java.lang.String v_idApp, java.lang.String v_nom, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.deleteMenuApps(v_idApp, v_nom, v_usuario, v_ip);
  }
  
  public boolean updateRolApps(java.lang.String v_App, java.lang.String v_rol, java.lang.String v_nom, java.lang.String v_st, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateRolApps(v_App, v_rol, v_nom, v_st, v_usuario, v_ip);
  }
  
  public boolean deleteRolApps(java.lang.String v_idApp, java.lang.String v_rol, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.deleteRolApps(v_idApp, v_rol, v_usuario, v_ip);
  }
  
  public boolean accesoOpcion(java.lang.String v_app, int v_rol, java.lang.String v_opc) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.accesoOpcion(v_app, v_rol, v_opc);
  }
  
  public java.lang.String[] buscaAccesos(java.lang.String v_App, int v_Rol) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.buscaAccesos(v_App, v_Rol);
  }
  
  public java.lang.String[] buscaAccesosDesc(java.lang.String v_App, int v_Rol) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.buscaAccesosDesc(v_App, v_Rol);
  }
  
  public int buscaRol(java.lang.String v_App, java.lang.String v_Usu) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.buscaRol(v_App, v_Usu);
  }
  
  public boolean insertaAcceso(java.lang.String v_App, int v_rol, java.lang.String[] v_opc) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.insertaAcceso(v_App, v_rol, v_opc);
  }
  
  public boolean updateParamSec(int v_exp, int v_ses, int v_sus, int v_int, int v_avi, int v_car1, int v_car2, int v_log, java.lang.String v_usu, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateParamSec(v_exp, v_ses, v_sus, v_int, v_avi, v_car1, v_car2, v_log, v_usu, v_ip);
  }
  
  public java.lang.String[] getUsersApps(java.lang.String v_app) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getUsersApps(v_app);
  }
  
  public boolean updateCuenta(java.lang.String v_usu, java.lang.String v_celular, java.lang.String v_email, java.lang.String v_usuario, java.lang.String v_ip) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.updateCuenta(v_usu, v_celular, v_email, v_usuario, v_ip);
  }
  
  public java.lang.String[] buscaAccesosDesarrollador(java.lang.String v_App) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.buscaAccesosDesarrollador(v_App);
  }
  
  public boolean insertaDesarrollador(java.lang.String v_App, java.lang.String[] v_ingenieros, int cant) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.insertaDesarrollador(v_App, v_ingenieros, cant);
  }
  
  public boolean autenticaToken(java.lang.String v_usr, java.lang.String v_app, long v_token) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.autenticaToken(v_usr, v_app, v_token);
  }
  
  public java.lang.String URLRetorno(java.lang.String v_usr) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.URLRetorno(v_usr);
  }
  
  
}