$(document).ready(function(){
	$("#btnSearch").click(loadTarjeta);
	$("#searchTarjeta").keyup(function(event){
	    if (event.keyCode == 13) {
	        loadTarjeta();
	    }
	}).focus();
	$("#activateDiv").dialog({
		autoOpen:false,
		modal: true,
		width:400,
		buttons: {
			Aceptar:function(){
				var deviceData = {
					id_telefono:$("#id_telefono").text(),
					tarjeta:$("#tarjeta").text(),
					fecha_ini:$("#txtfecha_ini").val(),
					fecha_fin:$("#txtfecha_fin").val(),
					activo:$("#chkactive").prop("checked")
				};
				$.ajax({
					url:"ws/DeviceUpdate",
					type:"POST",
					data:deviceData,
					dataType:"json",
					success:function(data){
						$("#activateDiv").dialog("close");
						loadTarjeta();
						message(data.message);
					},
					error:function(a,b,c,d){
						message(a);
					}
				});
				
			},
			Cancelar:function(){
				$("#activateDiv").dialog("close");
			},
			Eliminar:function(){
				var deviceData = {
						id_telefono:$("#id_telefono").text(),
						tarjeta:$("#tarjeta").text(),
					};
				if (confirm("Esta seguro que desea eliminar?")){
					$.ajax({
						url:"ws/DeviceDelete",
						type:"POST",
						data:deviceData,
						dataType:"json",
						success:function(data){
							$("#activateDiv").dialog("close");
							loadTarjeta();
							message(data.message);
						},
						error:function(a,b,c,d){
							message(a);
						}

					});
				}
			}
		}
	});
	var datePicker = {dateFormat:"yy-mm-dd"};
	$("#txtfecha_ini").datepicker(datePicker);
	$("#txtfecha_fin").datepicker(datePicker);
});

function loadTarjeta(){
	var id = $("#searchTarjeta").val();
	if (id == ""){
		message	("debe introduccir una tarjeta");
		$("#searchTarjeta").focus();
		return;
	}
	$.ajax({
		url:"ws/loadTarjeta",
		type:"POST",
		data:{tarjeta:id},
		dataType:"json",
		success:function(data){
			switch(data.status){
			case 200:
				break;
			case 404:
				message("Tarjeta no encontrada");
				data.id_telefono = "";
				data.tarjeta = "";
				data.fecha_ini = "";
				data.fecha_fin = "";
				data.fecha_actual = "";
				data.actualizado = "";
				data.activo = "";
				break;
			case 500:
				message("Error procesando requerimiento");
				message(data.message);
				data.id_telefono = "";
				data.tarjeta = "";
				data.fecha_ini = "";
				data.fecha_fin = "";
				data.fecha_actual = "";
				data.actualizado = "";
				data.activo = "";
				break;
			}
			$("#id_telefono").text(data.id_telefono);
			$("#tarjeta").text(data.tarjeta);
			$("#fecha_ini").text(data.fecha_ini);
			$("#fecha_fin").text(data.fecha_fin);
			$("#fecha_actual").text(data.fecha_actual);
			$("#actualizdo").text(data.actualizado);
			if (data.activo == 1){
				$("#activo").text("Activo");
			} else if (data.activo != ""){
				$("#activo").text("Inactivo");
			}
		}
	});
}

function sendMessage(mess,type,data){
	if (!deviceSelected())
		return;
	var id = $("#id_telefono").text();
	$.ajax({
		url:"ws/sendMessage",
		type:"POST",
		data:{device:id,message:mess, type:type,data:data},
		dataType:"json",
		success:function(data){
			message(data.status);
		},
		error:function(a,b,c,d){
			message(a);
		}
	});
}

function activateDevice(){
	if (!deviceSelected())
		return;
	var fecha_ini = textToDate($("#fecha_ini").text());
	var fecha_fin = textToDate($("#fecha_fin").text());
	$("#txtfecha_ini").val(fecha_ini);
	$("#txtfecha_fin").val(fecha_fin);
	if ($("#activo").text() == "Inactivo"){
		$("#chkactive").prop("checked",false);
	} else {
		$("#chkactive").prop("checked",true);
	}
	
	$("#activateDiv").dialog("open");
}

function deviceSelected(){
	if($("#id_telefono").text() === ""){
		message("debe seleccionar un dispositivo");
		$("#searchTarjeta").focus();
		return false;
	}
	return true;
}
var messageClock;
function message(text){
	$("#statusText").text(text);
	
	if (text != ''){
		messageClock = window.setInterval("message('')", 2000);	
	} else {
		clearInterval(messageClock);
	}
	
}

function textToDate(datetext){
	var separator = "-";
	var d = new Date(Date.parse(datetext));
    var curr_date = d.getDate();
    if (curr_date < 10) curr_date = "0" + curr_date;
    var curr_month = d.getMonth() + 1; //Months are zero based
    if (curr_month < 10) curr_month = "0" + curr_month;
    var curr_year = d.getFullYear();
    
    return curr_year + separator + curr_month + separator + curr_date;
}