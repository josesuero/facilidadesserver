<%@page import="org.tempuri.Service1SoapProxy"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	//Verificar session
	//Verificar parametros
	//webservice Portal verificar validez
	//guardar session
	//fallo redireccionar a portal

	if (request.getParameter("logout") != null){
		session.removeAttribute("usr");
	}

	if (session.getAttribute("usr") == null || session.getAttribute( "usr" ).equals("")){

		if (request.getParameter("usr") != null && !request.getParameter("usr").equals("")){

			String usr = request.getParameter("usr");
			int token = Integer.parseInt(request.getParameter("token"));
			String app = request.getParameter("app");

			Service1SoapProxy service = new Service1SoapProxy();
			if (service.autenticaToken(usr, app, token)){
				session.setAttribute("usr", usr);
			} else {
				//TODO: activate portal
				//response.sendRedirect("http://nttappsweb0001/userportal/");
			}
		} else {
			//TODO: activate portal
			//response.sendRedirect("http://nttappsweb0001/userportal/");
		}
	}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CELTEC</title>
<script type="text/javascript" src="js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.0.custom.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<link rel="stylesheet" type="text/css" href="css/redmond/jquery-ui-1.10.0.custom.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body topmargin="0" leftmargin="0">
	
	<table style="width:100%" border="1">
		<tr class="header">
			<td colspan="2" >
				<!--Header Start-->
				<table style="width:100%">
					<tr>
						<td>Logo</td>
						<td align="right">
							<!-- UserInfo Start-->
							<table>
								<tr>
									<td>Usuario</td>
								</tr>
								<tr>
									<td>Info</td>
								</tr>
							</table>
							<!-- UserInfo End --->
						</td>
					</tr>
				</table>				
				<!--Header End --->
			</td>
		</tr>
		<tr>
			<td>
				<!--Body Start-->
				<table>
					<tr >
						<td valign="middle">
							Tarjeta: 
						</td>
						<td>
							<input type="text" id="searchTarjeta" />
							<button id="btnSearch">Buscar</button>
						</td>
						<td>
							<span id="statusText"></span>
						</td>
					</tr>
				</table>
				<p style="font-weight:bold">Informaci&oacute;n:</p>
				<table>
					<tr>
						<td >
							Tarjeta:
						</td>
						<td>
							<span id="tarjeta"></span>
						</td>
					</tr>
					<tr>
						<td>
							Tel&eacute;fono
						</td>
						<td>
							<span id="id_telefono" ></span>
						</td>
					</tr>
					<tr>
						<td>
							Fecha Inicial:
						</td>
						<td>
							<span id="fecha_ini" ></span>
						</td>
					</tr>
					<tr>
						<td>
							Fecha Final:
						</td>
						<td>
							<span id="fecha_fin" ></span>
						</td>
					</tr>
					<tr>
						<td>
							Creado:
						</td>
						<td>
							<span id="fecha_actual" ></span>
						</td>
					</tr>
					<tr>
						<td>
							Activo:
						</td>
						<td>
							<span id="activo" ></span>
						</td>
					</tr>
				</table>
				<!--Body End --->
			</td>
			<td style="width:200px">
				<!--ToolColumn Start-->
				<table align="left">
					<tr>
						<td>
							<button onclick='sendMessage("Mensaje de prueba","info","[]");'>Mensaje de Prueba</button>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td>
							<button onclick='sendMessage("Sus Ordenes fueron actualizadas","OrdersGetAll","[]");'>Actualizar Ordenes</button>
						</td>
					</tr>
					<tr>
						<td>
							<button onclick='sendMessage("Delete Data","deleteData","[]");'>Eliminar Aplicacion</button>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td>
							<button onclick="activateDevice();">Modificar</button>
						</td>
					</tr>
				</table>
				<!--ToolColumn End --->
			</td>
		</tr>
	</table>
	<div id="activateDiv" title="Activar">
		<table>
			<tr>
				<td><label for="chkactive">Activo</label></td>
				<td>
					<input type="checkbox" id="chkactive"/>
				</td>
			</tr>
			<tr>
				<td><label for="txtfecha_ini">Fecha Inicial</label></td>
				<td>
					<input type="text" id="txtfecha_ini"/>
				</td>
			</tr>
			<tr>
				<td><label for="txtfecha_fin">Fecha Final</label></td>
				<td>
					<input type="text" id="txtfecha_fin"/>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>